! This file is part of GTC version 4.3.0
! GTC version 4.3.0 is released under the 3-Clause BSD license:

! Copyright (c) 2002,2010,2016, GTC Team (team leader: Zhihong Lin, zhihongl@uci.edu)
! All rights reserved.

! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are met:

! 1. Redistributions of source code must retain the above copyright notice,
!    this list of conditions and the following disclaimer.

! 2. Redistributions in binary form must reproduce the above copyright notice,
!    this list of conditions and the following disclaimer in the documentation
!    and/or other materials provided with the distribution.

! 3. Neither the name of the GTC Team nor the names of its contributors may be
!    used to endorse or promote products derived from this software without
!    specific prior written permission.



subroutine gather
  use system_env
  use global_parameters,only: mype,mpsi,mgrid,rho0,ihybrid,nhybrid,deltabpara,magnetic
  use particle_array
  use field_array,only: sfluidne,igrid,mtheta,gpsi200,diag_flux
  use conservative_scheme
  use magnetic_island
  implicit none

  interface
  subroutine gather_vdr_vap(zpart,zpart0,wppart,wtpart0,wtpart1,density,&
        flow,jtpart0,jtpart1,wzpart,zonal,zonalc,marker,markert,pmark,&
        qpart,apart,pload,ngyro,mp,choice,ihybrid,nhybrid,&
        pressurepara,pressureperp,sfluidn,dnsave,&
        switch)
      use precision
      implicit none

      integer pload,ngyro,mp
      integer choice
      integer,optional :: ihybrid,nhybrid
      integer,dimension(:,:) :: jtpart0,jtpart1
      real(lk) qpart,apart
      real(lk),dimension(:) :: wzpart,marker,markert
      real(lk),dimension(0:) :: zonal,zonalc,pmark
      real(lk),dimension(:,:) :: wppart,wtpart0,wtpart1
      real(lk),dimension(0:,:) :: density,flow
      real(lk),dimension(:,:) :: zpart,zpart0
      real(lk),dimension(:,:),optional :: pressurepara,pressureperp,sfluidn
      real(lk),dimension(:,:,:),optional :: dnsave
      character(*),intent(in),optional :: switch
    end subroutine gather_vdr_vap
  end interface

 
 integer i,j,ij

 magnetic=1

      call  gather_vdr_vap(zion,zion0,wpion,wtion0,wtion1,&
      densityi,flowi,jtion0,jtion1,wzion,zonali,zonalci,&
      markeri,markerit,pmarki,qion,aion,iload,ngyroi,mi,0,&
      ihybrid=ihybrid,nhybrid=nhybrid,pressurepara=pressureipara,&
      pressureperp=pressureiperp,sfluidn=sfluidne,dnsave=dnesave)

!pressurepara and pressureperp are temporal variable
!$omp parallel do private(i,j,ij)
do i=0,mpsi
   do j=0,mtheta(i)
      ij=igrid(i)+j
   flux_fks_i(1,:,ij)=sqrt(densityi(:,ij)**2+flowi(:,ij)**2)
   flux_fks_i(2,:,ij)=sqrt(pressureipara(:,ij)**2+pressureiperp(:,ij)**2)
   flux_fks_i(3,:,ij)=sqrt((densityi(:,ij)+pressureipara(:,ij))**2+(flowi(:,ij)+pressureiperp(:,ij))**2)
   flux_fks_i(4,:,ij)=flowi(:,ij)+pressureiperp(:,ij)

  sum_flux_i(1,:,ij)=sum_flux_i(1,:,ij)+flux_fks_i(1,:,ij)
  sum_flux_i(2,:,ij)=sum_flux_i(2,:,ij)+flux_fks_i(2,:,ij)
  sum_flux_i(3,:,ij)=sum_flux_i(3,:,ij)+flux_fks_i(3,:,ij)
  sum_flux_i(4,:,ij)=sum_flux_i(4,:,ij)+flux_fks_i(4,:,ij)

   enddo
enddo

           call  gather_vdr_vap(zfaste,zfaste0,wpfaste,wtfaste0,wtfaste1,&
      densityfe,flowfe,jtfaste0,jtfaste1,wzfaste,zonalfe,zonalcfe,&
      markerfe,markerfet,pmarkfe,qfaste,afaste,feload,ngyrofe,mfe,0,&
      ihybrid=ihybrid,nhybrid=nhybrid,pressurepara=pressurefastepara,&
      pressureperp=pressurefasteperp,sfluidn=sfluidne,dnsave=dnesave)

!$omp parallel do private(i,j,ij)
do i=0,mpsi
   do j=0,mtheta(i)
      ij=igrid(i)+j
  flux_fks_e(1,:,ij)=sqrt(densityfe(:,ij)**2+flowfe(:,ij)**2)
  flux_fks_e(2,:,ij)=sqrt(pressurefastepara(:,ij)**2+pressurefasteperp(:,ij)**2)
  flux_fks_e(3,:,ij)=sqrt((densityfe(:,ij)+pressurefastepara(:,ij))**2+(flowfe(:,ij)+pressurefasteperp(:,ij))**2)
  flux_fks_e(4,:,ij)=flowfe(:,ij)+pressurefasteperp(:,ij)

  sum_flux_e(1,:,ij)=sum_flux_e(1,:,ij)+flux_fks_e(1,:,ij)
  sum_flux_e(2,:,ij)=sum_flux_e(2,:,ij)+flux_fks_e(2,:,ij)
  sum_flux_e(3,:,ij)=sum_flux_e(3,:,ij)+flux_fks_e(3,:,ij)
  sum_flux_e(4,:,ij)=sum_flux_e(4,:,ij)+flux_fks_e(4,:,ij)

enddo
enddo



!--------------------------------------------------------------------------
      call  gather_vdr_vap(zion,zion0,wpion,wtion0,wtion1,&
      densityi,flowi,jtion0,jtion1,wzion,zonali,zonalci,&
      markeri,markerit,pmarki,qion,aion,iload,ngyroi,mi,1,&
      ihybrid=ihybrid,nhybrid=nhybrid,pressurepara=pressureipara,&
      pressureperp=pressureiperp,sfluidn=sfluidne,dnsave=dnesave)

!pressurepara and pressureperp are temporal variable
!$omp parallel do private(i,j,ij)
do i=0,mpsi
   do j=0,mtheta(i)
      ij=igrid(i)+j

   flux_fks_i(5,:,ij)=sqrt(densityi(:,ij)**2+flowi(:,ij)**2)
   flux_fks_i(6,:,ij)=sqrt(pressureipara(:,ij)**2+pressureiperp(:,ij)**2)
   flux_fks_i(7,:,ij)=sqrt((densityi(:,ij)+pressureipara(:,ij))**2+(flowi(:,ij)+pressureiperp(:,ij))**2)
   flux_fks_i(8,:,ij)=flowi(:,ij)+pressureiperp(:,ij)


  sum_flux_i(5,:,ij)=sum_flux_i(5,:,ij)+flux_fks_i(5,:,ij)
  sum_flux_i(6,:,ij)=sum_flux_i(6,:,ij)+flux_fks_i(6,:,ij)
  sum_flux_i(7,:,ij)=sum_flux_i(7,:,ij)+flux_fks_i(7,:,ij)
  sum_flux_i(8,:,ij)=sum_flux_i(8,:,ij)+flux_fks_i(8,:,ij)
  sum_flux_i(9,:,ij)=sum_flux_i(9,:,ij)+1.0

   enddo
enddo


           call  gather_vdr_vap(zfaste,zfaste0,wpfaste,wtfaste0,wtfaste1,&
      densityfe,flowfe,jtfaste0,jtfaste1,wzfaste,zonalfe,zonalcfe,&
      markerfe,markerfet,pmarkfe,qfaste,afaste,feload,ngyrofe,mfe,1,&
      ihybrid=ihybrid,nhybrid=nhybrid,pressurepara=pressurefastepara,&
      pressureperp=pressurefasteperp,sfluidn=sfluidne,dnsave=dnesave)

!$omp parallel do private(i,j,ij)
do i=0,mpsi
   do j=0,mtheta(i)
      ij=igrid(i)+j

  flux_fks_e(1,:,ij)=sqrt(densityfe(:,ij)**2+flowfe(:,ij)**2)
  flux_fks_e(2,:,ij)=sqrt(pressurefastepara(:,ij)**2+pressurefasteperp(:,ij)**2)
  flux_fks_e(3,:,ij)=sqrt((densityfe(:,ij)+pressurefastepara(:,ij))**2+(flowfe(:,ij)+pressurefasteperp(:,ij))**2)
  flux_fks_e(4,:,ij)=flowfe(:,ij)+pressurefasteperp(:,ij)

  sum_flux_e(5,:,ij)=sum_flux_e(5,:,ij)+flux_fks_e(5,:,ij)
  sum_flux_e(6,:,ij)=sum_flux_e(6,:,ij)+flux_fks_e(6,:,ij)
  sum_flux_e(7,:,ij)=sum_flux_e(7,:,ij)+flux_fks_e(7,:,ij)
  sum_flux_e(8,:,ij)=sum_flux_e(8,:,ij)+flux_fks_e(8,:,ij)
  sum_flux_e(9,:,ij)=sum_flux_e(9,:,ij)+1.0
enddo
enddo

magnetic=0

end subroutine gather


subroutine gather_vdr_vap(zpart,zpart0,wppart,wtpart0,wtpart1,density,flow,&
    jtpart0,jtpart1,wzpart,zonal,zonalc,marker,markert,pmark,qpart,apart,&
    pload,ngyro,mp,choice,ihybrid,nhybrid,pressurepara,pressureperp,&
    sfluidn,dnsave,switch)
  use system_env
  use precision, only: lk, mpi_Rsize
  use global_parameters,only: magnetic,npartdom,partd_comm,left_pe,&
    right_pe,myrank_toroidal,toroidal_comm,mtoroidal,irk,BC_field,&
    istep,mstep,nfilter,mpsi,mgrid,antenna_on,etemp0,eden0,r0,mype,izonal,deltabpara
  use field_array,only: igrid,mtheta,itran,rhom,guzz,nmodes,nmode,zeta0
  use equilibrium,only: lsp,spdpsi_inv,spdpsi,lst,spdtheta_inv,spdtheta,&
    bsp,xsp,spdim
  implicit none

!TODO: check decleartions
!declaration of the dummy arguments
  integer pload,ngyro,mp
  integer choice
  integer,optional :: ihybrid,nhybrid
  integer,dimension(:,:) :: jtpart0,jtpart1
  real(lk) qpart,apart
  real(lk),dimension(:) :: wzpart,marker,markert
  real(lk),dimension(0:) :: zonal,zonalc,pmark
  real(lk),dimension(:,:) :: wppart,wtpart0,wtpart1
  real(lk),dimension(0:,:) :: density,flow
  real(lk),dimension(:,:) :: zpart,zpart0
  real(lk),dimension(0:,:),optional :: pressurepara,pressureperp,sfluidn
  real(lk),dimension(0:,:,:),optional :: dnsave
  character(*),intent(in),optional :: switch

!declaration of the local variables
  integer m,i,j,jt,ii,igyro,ij,isp,jst,&
    icount,idest,isource,isendtag,irecvtag,istatus(MPI_STATUS_SIZE),ia
  real(lk) gyrodum,weight,b,upara,cmratio,wz1,wz0,wp1,wp0,wt11,wt10,wt01,&
    wt00,adum(0:mpsi),dnitmp(0:1,mgrid),djitmp(0:1,mgrid),dpx,dp2,dzx,&
    dx(27),pdum,tdum,dtx,dt2,zf0,zc0,temp,majorr,dpitmppara(0:1,mgrid),&
    dpitmpperp(0:1,mgrid),energypara,energyperp,vdr,vdr_turb,vap,ppara00(0:mpsi),pperp00(0:mpsi)

  real(lk) sendl(mgrid,4),recvr(mgrid,4)
# 68

# 71

  real(lk) zetagyravg,kperprhoi

!  zetagyravg=1.0
!  kperprhoi=0.0

  cmratio=qpart/apart
# 80

!$omp parallel do private(ij)

  do ij=1,mgrid
    density(0:1,ij)=0.0_lk
    flow(0:1,ij)=0.0_lk

    pressurepara(0:1,ij)=0.0_lk
    pressureperp(0:1,ij)=0.0_lk

if(deltabpara==1)then
    pressureperp(0:1,ij)=0.0_lk
endif
  enddo
!$acc end parallel
  
  if(magnetic==0)then
# 99

! scatter particle density for electrostatic simulation
!$omp parallel do&
!$omp& private(m,igyro,weight,wz1,wz0,wp1,wp0,wt10,wt00,wt11,wt01,ij,dzx,dx,&
!$omp& b,ii,majorr,kperprhoi,zetagyravg,pdum,isp,dpx,tdum,jst,dtx,dt2,dp2)&
!$omp& reduction(+: density)

     do m=1,mp

        pdum=zpart(1,m)
        isp=max(1,min(lsp-1,ceiling(pdum*spdpsi_inv)))
        dpx=pdum-spdpsi*real(isp-1)
! radial spline of b avoids sigularity near axis:
! y=y1+y2*sqrt(x)+y3*x for psi<spdpsi
        if(isp==1)dpx=sqrt(dpx)
        dp2=dpx*dpx

        tdum=zpart(2,m)
        jst=max(1,min(lst-1,ceiling(tdum*spdtheta_inv)))
        dtx=tdum-spdtheta*real(jst-1)
        dt2=dtx*dtx
        dzx=zpart(3,m)-zeta0

        dx(1)=1.0_lk
        dx(2)=dpx
        dx(3)=dp2
        dx(4:6)=dx(1:3)*dtx
        dx(7:9)=dx(1:3)*dt2
        dx(10:18)=dx(1:9)*dzx
        dx(19:27)=dx(1:9)*dzx*dzx

        b=0.0_lk
        majorr=0.0_lk
        do ii = 1, spdim
          b=b +bsp(ii,isp,jst)*dx(ii)
          majorr=majorr +xsp(ii,isp,jst)*dx(ii)
        enddo

        zetagyravg=1.0_lk
# 144

        weight=zpart(5,m)*zetagyravg
     
        wz1=weight*wzpart(m)      !weight for upper toroidal grid
        wz0=weight-wz1            !weight for lower toroidal grid
        do igyro=1,ngyro
          wp1=wppart(igyro,m)     !outer flux surface
          wp0=1.0_lk-wp1             !inner flux surface

          wt10=wp0*wtpart0(igyro,m) !upper poloidal grid on inner flux surface
          wt00=wp0-wt10           !lower poloidal grid on inner flux surface
           
          wt11=wp1*wtpart1(igyro,m) !upper poloidal grid on outer flux surface
          wt01=wp1-wt11           !lower poloidal grid on outer flux surface

! If no loop-level parallelism, write directly into array "density()"
          ij=jtpart0(igyro,m)     !lower poloidal grid on inner flux surface
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt00 !lower toroidal grid
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt00 !upper toroidal grid
           
          ij=ij+1                 !upper poloidal grid on inner flux surface
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt10
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt10
           
          ij=jtpart1(igyro,m)     !lower poloidal grid on outer flux surface
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt01
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt01
           
          ij=ij+1                 !upper poloidal grid on outer flux surface
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt11
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt11
        enddo
      enddo
    else
      if(deltabpara==1)then
! scatter particle density and flow for electromagnetic simulation
# 190


!$omp parallel do private(m,igyro,pdum,isp,dpx,dp2,tdum,jst,dtx,dt2,b,&
!$omp& upara,weight,wz1,wz0,wp1,wp0,wt10,wt00,wt11,wt01,ij)&
!$omp& reduction(+: density,flow)&
!$omp& private(energypara,energyperp)&
!$omp& reduction(+: pressurepara,pressureperp)
# 203


     do m=1,mp

! 2D spline in (psi, theta)
        pdum=zpart(1,m)
        isp=max(1,min(lsp-1,ceiling(pdum*spdpsi_inv)))
        dpx=pdum-spdpsi*real(isp-1)

        tdum=zpart(2,m)
        jst=max(1,min(lst-1,ceiling(tdum*spdtheta_inv)))
        dtx=tdum-spdtheta*real(jst-1)
        dt2=dtx*dtx

! radial spline of b avoids sigularity near axis:
! y=y1+y2*sqrt(x)+y3*x for psi<spdpsi
        if(isp==1)dpx=sqrt(dpx)
        dp2=dpx*dpx

        b=    bsp(1,isp,jst)+bsp(2,isp,jst)*dpx+bsp(3,isp,jst)*dp2+ &
             (bsp(4,isp,jst)+bsp(5,isp,jst)*dpx+bsp(6,isp,jst)*dp2)*dtx+ &
             (bsp(7,isp,jst)+bsp(8,isp,jst)*dpx+bsp(9,isp,jst)*dp2)*dt2
       
!b=0.0
!do ii=1,spdim
!b=b+bsp(ii,isp,jst)*dx(ii)
!enddo
        upara=zpart(4,m)*b*cmratio !parallel velocity

        energypara=apart*upara*upara

        energyperp=zpart(6,m)*zpart(6,m)*b
        weight=zpart(5,m)
        
        wz1=weight*wzpart(m)
        wz0=weight-wz1     
!Todo: why reduction not work for the following loop?
!!$acc loop reduction(+:density,flow)
!$acc loop seq
        do igyro=1,ngyro
          wp1=wppart(igyro,m)       !outer flux surface
          wp0=1.0-wp1               !inner flux surface
           
          wt10=wp0*wtpart0(igyro,m)
          wt00=wp0-wt10
           
          wt11=wp1*wtpart1(igyro,m)
          wt01=wp1-wt11

! If no loop-level parallelism, use original algorithm (write
! directly into array "density()".
          ij=jtpart0(igyro,m)
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt00
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt00
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt00*upara
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt00*upara

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt00*energypara
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt00*energypara
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt00*energyperp
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt00*energyperp

         
          ij=ij+1
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt10
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt10
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt10*upara
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt10*upara

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt10*energypara
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt10*energypara
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt10*energyperp
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt10*energyperp

    
          ij=jtpart1(igyro,m)
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt01
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt01
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt01*upara
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt01*upara

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt01*energypara
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt01*energypara
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt01*energyperp
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt01*energyperp

           
          ij=ij+1
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt11
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt11
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt11*upara
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt11*upara

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt11*energypara
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt11*energypara
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt11*energyperp
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt11*energyperp

        enddo

# 373

        enddo
!$acc end parallel
else
# 379


!$omp parallel do private(m,igyro,pdum,isp,dpx,dp2,tdum,jst,dtx,dt2,b,&
!$omp& upara,weight,wz1,wz0,wp1,wp0,wt10,wt00,wt11,wt01,ij)&
!$omp& reduction(+: density,flow)&
!$omp& private(energypara,energyperp,vdr,vdr_turb,vap)&
!$omp& reduction(+: pressurepara,pressureperp)
# 390


     do m=1,mp

! 2D spline in (psi, theta)
        pdum=zpart(1,m)
        isp=max(1,min(lsp-1,ceiling(pdum*spdpsi_inv)))
        dpx=pdum-spdpsi*real(isp-1)

        tdum=zpart(2,m)
        jst=max(1,min(lst-1,ceiling(tdum*spdtheta_inv)))
        dtx=tdum-spdtheta*real(jst-1)
        dt2=dtx*dtx

! radial spline of b avoids sigularity near axis:
! y=y1+y2*sqrt(x)+y3*x for psi<spdpsi
        if(isp==1)dpx=sqrt(dpx)
        dp2=dpx*dpx

        b=    bsp(1,isp,jst)+bsp(2,isp,jst)*dpx+bsp(3,isp,jst)*dp2+ &
             (bsp(4,isp,jst)+bsp(5,isp,jst)*dpx+bsp(6,isp,jst)*dp2)*dtx+ &
             (bsp(7,isp,jst)+bsp(8,isp,jst)*dpx+bsp(9,isp,jst)*dp2)*dt2
        
        upara=zpart(4,m)*b*cmratio !parallel velocity

        energypara=apart*upara*upara
        energyperp=zpart(6,m)*zpart(6,m)*b

        vdr=zpart(8,m)
        vdr_turb=zpart(9,m)
        vap=zpart0(8,m)

        weight=zpart(5,m)

        if(choice==1)then
        weight=1.0
        endif        


        wz1=weight*wzpart(m)
        wz0=weight-wz1     
!Todo: why reduction not work for the following loop?
!!$acc loop reduction(+:density,flow)
!$acc loop seq
        do igyro=1,ngyro
          wp1=wppart(igyro,m)       !outer flux surface
          wp0=1.0_lk-wp1               !inner flux surface
           
          wt10=wp0*wtpart0(igyro,m)
          wt00=wp0-wt10
           
          wt11=wp1*wtpart1(igyro,m)
          wt01=wp1-wt11

! If no loop-level parallelism, use original algorithm (write
! directly into array "density()".
          ij=jtpart0(igyro,m)
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt00*zpart(8,m)
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt00*zpart(8,m)
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt00*zpart(9,m)
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt00*zpart(9,m)

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt00*zpart0(8,m)
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt00*zpart0(8,m)
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt00*zpart0(9,m)
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt00*zpart0(9,m)

         
          ij=ij+1
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt10*zpart(8,m)
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt10*zpart(8,m)
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt10*zpart(9,m)
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt10*zpart(9,m)

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt10*zpart0(8,m)
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt10*zpart0(8,m)
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt10*zpart0(9,m)
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt10*zpart0(9,m)

    
          ij=jtpart1(igyro,m)
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt01*zpart(8,m)
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt01*zpart(8,m)
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt01*zpart(9,m)
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt01*zpart(9,m)

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt01*zpart0(8,m)
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt01*zpart0(8,m)
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt01*zpart0(9,m)
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt01*zpart0(9,m)

           
          ij=ij+1
!$acc atomic update
          density(0,ij) = density(0,ij) + wz0*wt11*zpart(8,m)
!$acc atomic update
          density(1,ij) = density(1,ij) + wz1*wt11*zpart(8,m)
!$acc atomic update
          flow(0,ij) = flow(0,ij) + wz0*wt11*zpart(9,m)
!$acc atomic update
          flow(1,ij) = flow(1,ij) + wz1*wt11*zpart(9,m)

!$acc atomic update
          pressurepara(0,ij) = pressurepara(0,ij) + wz0*wt11*zpart0(8,m)
!$acc atomic update
          pressurepara(1,ij) = pressurepara(1,ij) + wz1*wt11*zpart0(8,m)
!$acc atomic update
          pressureperp(0,ij) = pressureperp(0,ij) + wz0*wt11*zpart0(9,m)
!$acc atomic update
          pressureperp(1,ij) = pressureperp(1,ij) + wz1*wt11*zpart0(9,m)

        enddo
     enddo
!$acc end parallel
endif  
endif


!$acc update host(density,flow)



!$acc update host(pressurepara,pressureperp)

# 533

! If we have a particle decomposition on the toroidal domains, do a reduce
! operation to add up all the contributions to charge density on the grid
  if(npartdom>1)then
!$omp parallel do private(ij)
    do ij=1,mgrid
      dnitmp(0:1,ij)=density(0:1,ij)
      density(0:1,ij)=0.0_lk
      djitmp(0:1,ij)=flow(0:1,ij)
      flow(0:1,ij)=0.0_lk
    enddo
    icount=2*mgrid
    call MPI_ALLREDUCE(dnitmp,density,icount,mpi_Rsize,MPI_SUM,partd_comm,ierror)
    call MPI_ALLREDUCE(djitmp,flow,icount,mpi_Rsize,MPI_SUM,partd_comm,ierror)


!$omp parallel do private(ij)
    do ij=1,mgrid
      dpitmppara(0:1,ij)=pressurepara(0:1,ij)
      pressurepara(0:1,ij)=0.0_lk
      dpitmpperp(0:1,ij)=pressureperp(0:1,ij)
      pressureperp(0:1,ij)=0.0_lk
    enddo
    call MPI_ALLREDUCE(dpitmppara,pressurepara,icount,mpi_Rsize,MPI_SUM,partd_comm,ierror)
    call MPI_ALLREDUCE(dpitmpperp,pressureperp,icount,mpi_Rsize,MPI_SUM,partd_comm,ierror)
# 567

  endif

! poloidal end cell, discard ghost cell j=0
!$omp parallel do private(i)
  do i=0,mpsi
    density(:,igrid(i)+mtheta(i))=density(:,igrid(i)+mtheta(i))+density(:,igrid(i))
    flow(:,igrid(i)+mtheta(i))=flow(:,igrid(i)+mtheta(i))+flow(:,igrid(i))

    pressurepara(:,igrid(i)+mtheta(i))=pressurepara(:,igrid(i)+mtheta(i))+pressurepara(:,igrid(i))
    pressureperp(:,igrid(i)+mtheta(i))=pressureperp(:,igrid(i)+mtheta(i))+pressureperp(:,igrid(i))
# 582

  enddo

! toroidal end cell
!$omp parallel do private(i)
  do i=1,mgrid
    sendl(i,1)=density(0,i)
    sendl(i,2)=flow(0,i)
    recvr(i,1:2)=0.0_lk

    sendl(i,3)=pressurepara(0,i)
    sendl(i,4)=pressureperp(0,i)
    recvr(i,3:4)=0.0_lk
# 602

  enddo


  icount=4*mgrid
# 609

 if(deltabpara==1)then
  icount=4*mgrid
 endif
  idest=left_pe
  isource=right_pe
  isendtag=myrank_toroidal
  irecvtag=isource

! send density to left and receive from right
  call MPI_SENDRECV(sendl,icount,mpi_Rsize,idest,isendtag,&
       recvr,icount,mpi_Rsize,isource,irecvtag,toroidal_comm,istatus,ierror)
     
  if(myrank_toroidal == mtoroidal-1)then
! B.C. at zeta=2*pi is shifted
!$omp parallel do private(i,ii,jt)
    do i=0,mpsi
      ii=igrid(i)
      jt=mtheta(i)
      density(1,ii+1:ii+jt)=density(1,ii+1:ii+jt)+cshift(recvr(ii+1:ii+jt,1),itran(i))
      flow(1,ii+1:ii+jt)=flow(1,ii+1:ii+jt)+cshift(recvr(ii+1:ii+jt,2),itran(i))

      pressurepara(1,ii+1:ii+jt)=pressurepara(1,ii+1:ii+jt)+&
           cshift(recvr(ii+1:ii+jt,3),itran(i))
      pressureperp(1,ii+1:ii+jt)=pressureperp(1,ii+1:ii+jt)+&
           cshift(recvr(ii+1:ii+jt,4),itran(i))
# 640

    enddo
  else
! B.C. at zeta<2*pi is continuous
!$omp parallel do private(i,ii,jt)
    do i=1,mgrid
      density(1,i)=density(1,i)+recvr(i,1)
      flow(1,i)=flow(1,i)+recvr(i,2)

      pressurepara(1,i)=pressurepara(1,i)+recvr(i,3)
      pressureperp(1,i)=pressureperp(1,i)+recvr(i,4)
# 655

    enddo
  endif
  
! Commented out Valentin Aslanyan Oct 31 2017 - antenna paradigm changed
!  !self-consistent density from external antenna potential
!  if(present(switch) .and. switch=='density modification' .and. istep==1)then
!    if (antenna_on==1)then
!      do i=0,mpsi
!        temp=(etemp0)/(eden0)*7.43e2*7.43e2/(r0*r0)
!        do j=1,mtheta(i)
!          ij=igrid(i)+j
!          density(1,ij)=dn_ext(1,ij)*temp*real(pmark(i)/&
!            (mtheta(i)*mtoroidal))+density(1,ij)
!        enddo
!      enddo
!    endif
!  endif


! flux surface average and normalization
  gyrodum=1.0_lk/real(ngyro)
!$omp parallel do private(i,j,ij)
  do i=0,mpsi
    zonal(i)=0.0_lk
    zonalc(i)=0.0_lk
    do j=1,mtheta(i)
      ij=igrid(i)+j
      density(1,ij)=gyrodum*density(1,ij)
      zonal(i)=zonal(i)+density(1,ij)
      flow(1,ij)=gyrodum*flow(1,ij)
      zonalc(i)=zonalc(i)+flow(1,ij)
      pressureperp(1,ij)=gyrodum*pressureperp(1,ij)
      pressurepara(1,ij)=gyrodum*pressurepara(1,ij)
    enddo
  enddo

! time-averaged # of marker particles per cell ****This is currently not being used. Keep for future?
!if(irk==2 .and. ( .not. present(nhybrid) .or. present(nhybrid) .and. ihybrid==nhybrid))markert=markert+density(1,:)

!$omp parallel do private(i,j,ij)
  do i=0,mpsi
    do j=1,mtheta(i)
      ij=igrid(i)+j
      density(1,ij)=density(1,ij)*marker(ij)
      flow(1,ij)=flow(1,ij)*marker(ij)

      pressurepara(1,ij)=pressurepara(1,ij)*marker(ij)
      pressureperp(1,ij)=pressureperp(1,ij)*marker(ij)
# 709

    enddo
  enddo
  
! global sum of zonal modes (flux surface averaged), broadcast to every toroidal PE
  call MPI_ALLREDUCE(zonal,adum,mpsi+1,mpi_Rsize,MPI_SUM,toroidal_comm,ierror)
  zonal=adum/pmark
  zf0=sum(adum)

  call MPI_ALLREDUCE(zonalc,adum,mpsi+1,mpi_Rsize,MPI_SUM,toroidal_comm,ierror)
  zonalc=adum/pmark
  zc0=sum(adum)

!density subtracted by (0,0) mode
!> @todo
!> zonal subtraction is always needed. magnetic and izonal controls are not
!> correct here. Commented out by L. Shi on Apr. 17, 2017. Need to be removed
!> in next release.
!if(magnetic==1)then
!$omp parallel do private(i,j,ij)
    do i=0,mpsi
!if(izonal>0)then
!        do j=1,mtheta(i)
!          ij=igrid(i)+j
!         density(1,ij)=density(1,ij)-zonal(i)
!         flow(1,ij)=flow(1,ij)-zonalc(i)
!        enddo
!endif

! poloidal BC condition
      density(1,igrid(i))=density(1,igrid(i)+mtheta(i))
      flow(1,igrid(i))=flow(1,igrid(i)+mtheta(i))
      pressurepara(1,igrid(i))=pressurepara(1,igrid(i)+mtheta(i))
      pressureperp(1,igrid(i))=pressureperp(1,igrid(i)+mtheta(i)) 
   enddo
!endif

  if(pload/=9)then         ! For GK calculation
    if(pload<100)then
! enforce charge/momentum conservation for zonal flow/field mode in delta-f simulation
      zc0=zc0/sum(pmark)
      zf0=zf0/sum(pmark)
      zonal(1:mpsi-1)=zonal(1:mpsi-1)-zf0
      zonalc(1:mpsi-1)=zonalc(1:mpsi-1)-zc0
    else
! full-f: zonal flow subtracted by equilibrium density
      zonal=zonal-1.0
    endif
  else   ! For fully kinetic calculation
! full-f: zonal flow subtracted by equilibrium density
    zonal=zonal-1.0_lk
! This scheme is not correct for the delta-f simulation
  endif

!if(irk==2 .and. istep==mstep .and. ( .not. present(nhybrid) .or. present(nhybrid) .and. ihybrid==nhybrid)) then
!  markert=markert/real(mstep) ! # of marker particles per cell
!endif

! smooth particle density and current
  
# 773




  if(magnetic==1)then
    call periodicity(density)
    call periodicity(flow)
    call periodicity(pressurepara)
    call periodicity(pressureperp)
  endif
end subroutine gather_vdr_vap
