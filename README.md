# GTC nonlinear simulations of interactions between magnetic islands and microturbulence
This repository contains source codes and inputs that are used for electrostatic and electromagnetic GTC simulations of magnetic islands-turbulence interactions. The details of simulations models and results can be found at:

1. Electrostatic work:  https://doi.org/10.1063/1.5096962
2. Electromagnetic work: submitted to Plasma Science and Technology

---

## Electrostatic work 
This work uses drift kinetic electrons to study a prescibed island's nonlinear impacts on ITG turbulence, the code is developed based on GTC 4.3.0, to recover Kaisheng's 19 POP work, you need:

1. ES_code/no_flatten: for Kaisheng 19 POP (Figure. 1 & 2).
2. ES_code/with_flatten/stage_1: **STAGE 1** tests the magnetic island flattening effects without turbulence. (Figure. 3)
3. ES_code/with_flatten/stage_2: **STAGE 2** turns on turbulence to do a continue run based on the flattened profiles in **STAGE 1** (Figures. 4 to 7).

(**IMPORTANT**: To recover simulation results, you should first run STAGE 1 with gtc and input that is compiled in 2, and next do a **continue run** using the gtc and input that is compiled in 3.)  

---

## Electromagnetic work 

This work implements a conservative scheme with drift kinetic electrons for v_para formulation, the model is benchmarked for KAW and KBM physics and then used to study interactions between magnetic island and turbulence, to recover Kaisheng's 19 PST work, you need:
 
1. EM_code/kaw: kinetic alvfen waves test in a cylinder geometry using drift kientic electrons (Figure. 1&2).
2. EM_code/kbm: kinetic ballooning mode benchmark in a tokamak geometry using drift kientic electrons (Figure. 3&4).
3. EM_code/island: magentic islands-itg interactions using fluid electrons, the simulations are consisted of **TWO STAGE**, stage 1 builds the magnetic island flattened profiles without turbulence, stage 2 is a continue run with turbulence (Figure 5 to 7).

(**IMPORTANT**: To recover simulation results, in 3 you should first run a stage 1, and then do a **continue run** for stage 2.) 

---

## Contact

If you have any problems for code or papers, contact Kaisheng Fang with either:

1. wechat/phone: 18811521263
2. email: fkscreed@gmail.com

---
