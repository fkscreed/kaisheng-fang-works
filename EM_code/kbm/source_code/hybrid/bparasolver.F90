!> Purpose: calculaing delta B_para from particle information.
!> Input: string indicating whether non-adiabatic electron response is included
!in the calculation.
!"kinetic": with non-adiabatic electron response. "adiabatic": w/o non-adiabatic
!electron response 
!> Output: No output; just updates the array bpara.
!> Options: inversing=0 assumes k_perp*rho_s<<1. inversing=1 uses the Pade-like
!> approximation (Sec IV, G.Dong et al., POP, 2017) 
subroutine bparasolver(scenario)
  use precision
  use global_parameters
  use field_array
  use particle_array
  use petsc_array,only: luserb_bpara,luserx_bpara
  use equilibrium
  use interfaces
  implicit none

  integer m,i,j,ij,inversing
  real(lk) dtime,pdum,tdum,dpx,dp2,dtx,dt2,dbdt,dbdp,dbdz,&
           ri,g,b_inv,q_inv,&
           dentmp_bpara(mgrid),&
           dptdp,dptdt,dptdz,bpara0,&
           ne,te,dnedp,dtedp,er,wdrift0,web0,ddnedp,ddnedt,ddnedz,&
           ddpdp,ddpdt,ddpdz,dinddp,dinddt,dinddz,dpsidp,dpsidt,dpsidz,dne,&
           daparadp,daparadt,daparadz,dsdp,dphieffdp,dphieffdt,dphieffdz,&
           dkpparadp,dkpparadt,dkpparadz,dkpperpdp,dkpperpdt,dkpperpdz,&
           dkpparadpi,dkpparadti,dkpparadzi,dkpperpdpi,dkpperpdti,dkpperpdzi,gamma00
  character(*),intent(in) :: scenario

  dnedp=0.0_lk
  dtedp=0.0_lk

  inversing=1 !inversing=0 is assuming k_perp*rho_s<<1, inversing=1 is using the pade-like approximation
  !$omp parallel do private(i,j,ij,bpara0)
  do i=0,mpsi
    do j=1,mtheta(i)
      ij=igrid(i)+j
      bpara0=bpara(1,ij)
      bpara(1,ij)=-betae*(pressureiperp(1,ij)+sfluidne(1,ij)*meshte(i)-bpara0-sdeltapsi(1,ij)*meshte(i)*meshne(i)*kapate(i))/2.0
      if(scenario=='kinetic')then
        bpara(1,ij)=bpara(1,ij)-(1.0)*betae*(-densitye(1,ij)*meshte(i)+pressureeperp(1,ij))/2.0
      endif
      if(inversing==0)then
        bpara(1,ij)=bpara(1,ij)+betae*(densityi(1,ij)*meshni(i)*qion+sfluidne(1,ij)*qelectron)*meshte(1)
      else
        dentmp_bpara(ij)=(densityi(1,ij)*meshni(i)*qion+sfluidne(1,ij)*qelectron)
        bpara(1,ij)=bpara(1,ij)+betae*dentmp_bpara(ij)*meshte(1)/2.0
      endif
    enddo
    ! poloidal periodic condition, ghost cells need to be populated
    dentmp_bpara(igrid(i)) = dentmp_bpara(igrid(i)+mtheta(i))
  enddo

  if(inversing==1)then
    if(psi0>0.0_lk)dentmp_bpara(igrid(0):igrid(0)+mtheta(0))=0.0_lk
    dentmp_bpara(igrid(mpsi):igrid(mpsi)+mtheta(mpsi))=0.0_lk
    !$omp parallel do private(i)
    do i=mgridlow,mgridhigh
      luserb_bpara(i-mgridlow)=dentmp_bpara(i)
    enddo
#ifdef _PETSc
    call lapmat_pssolver_bpara
#endif
    !$omp parallel do private(i)
    do i=mgridlow,mgridhigh
      bpara(1,i)=bpara(1,i)+betae*meshte(1)*luserx_bpara(i-mgridlow)/2.0
    enddo
    !$omp parallel do private(i,j,ij)
    do i=0,mpsi
      do j=1,mtheta(i)
        ij=igrid(i)+j
        bpara(1,ij)=bpara(1,ij)*bmesh(ij)
      enddo
      bpara(1,igrid(i)) = bpara(1,igrid(i)+mtheta(i))
    enddo
  endif

  bpara(:,igrid(0):igrid(0)+mtheta(0))=0.0
  bpara(:,igrid(mpsi):igrid(mpsi)+mtheta(mpsi))=0.0
  if(nfilter>0)CALL FILTER(bpara)
  call smooth(bpara)

end subroutine bparasolver
