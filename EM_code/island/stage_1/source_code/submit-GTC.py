#!/usr/bin/env python

import sys
import os
import time
import subprocess as subp
import getopt
import argparse
import warnings
from datetime import datetime

# List for available machines
# Update this list when adding new machine supports
available_machine_list = ['cori', 'edison','titan']

def write_jobscript(args):
    r'''write out GTC job script for all acceptable machines'''
    if args.machine not in available_machine_list:
        raise ValueError('Machine [{0}] Not Supported.'.format(args.machine)+\
            ' Write your own job-script writer.'+\
            ' If an existing machine job-script is supported,'+\
            ' add current machine to the available list in submit-GTC.py.'+\
            ' Available machines are {0}'.format(available_machine_list))
    if args.nmpi is None:
        # calculate the total MPI processes based on total nodes,
        # OMP_threads, and machine type
        if args.machine == 'cori':
            ppn = 32
        elif args.machine == 'edison':
            ppn = 24
        elif args.machine == 'titan':
            ppn = 16
        else:
            raise ValueError('Wrong machine: {0}'.format(args.machine))

        if ppn % args.threads != 0:
            warnings.warn('OMP threads:{0}'.format(args.threads) +\
                          ' does not divide CPU per node:{0}'.format(ppn))
        mpi_per_node = int(ppn/args.threads)
        total_mpi = args.nodes*mpi_per_node
    else:
        total_mpi = args.nmpi
        mpi_per_node = int(float(total_mpi)/args.nodes)+1


    with open(args.job_filename, 'w+') as f:
        if (args.machine in ['cori', 'edison']):
            f.write('#!/bin/bash -l\n')
            f.write('#SBATCH --account={0}\n'.format(args.account))
            f.write('#SBATCH --partition={0}\n'.format(args.partition))
            f.write('#SBATCH --nodes={0}\n'.format(args.nodes))
            f.write('#SBATCH --time={0}\n'.format(args.time))
            f.write('#SBATCH --job-name={0}\n'.format(args.name))
            f.write('#SBATCH --workdir={0}\n'.format(args.workdir))
            if args.email is not None:
                f.write('#SBATCH --mail-user={0}\n'.format(args.email))
                f.write('#SBATCH --mail-type=ALL\n')
            if (args.machine == 'cori'):
                f.write('#SBATCH -C haswell\n')
        elif args.machine in ['titan']:
            f.write('#!/bin/sh \n')
            f.write('#PBS -A {0}\n'.format(args.account))
            f.write('#PBS -q {0}\n'.format(args.partition))
            f.write('#PBS -l nodes={0}\n'.format(args.nodes))
            f.write('#PBS -l walltime={0}\n'.format(args.time))
            f.write('#PBS -N {0}\n'.format(args.name))
            if args.email is not None:
                f.write('#PBS -M {0}\n'.format(args.email))
                f.write('#PBS -m abe\n') # send email for abort, begin and end
            f.write('\n')

        # OpenMP threads is by default always turned on
        f.write('export OMP_NUM_THREADS={0}\n\n'.format(args.threads))
        # Change directory to working dir
        if args.machine in ['titan']:
            f.write('cd $PBS_O_WORKDIR\n')
        # Create necessary directories
        f.write('mkdir -p restart_dir1\n')
        f.write('mkdir -p restart_dir2\n')
        f.write('mkdir -p restart_dir\n')
        f.write('mkdir -p phi_dir\n')
        f.write('mkdir -p trackp_dir\n\n')
        # Run the executable
        if (args.machine in ['cori', 'edison']):
            f.write('srun -n {0} -c $OMP_NUM_THREADS ./gtc\n'.format(total_mpi))
        elif args.machine in ['titan']:
            f.write('aprun -n {0} -N {1} -d $OMP_NUM_THREADS ./gtc\n'\
                    .format(total_mpi, mpi_per_node))
        f.write('\n')
        # Automatically increase irun by 1 in gtc.in
        f.write('irun_n=`sed -n \'s/^ *irun=\\([0-9]*\\).*/\\1/p\' gtc.in`'+'\n')
        f.write('(( irun_m = irun_n + 1 ))\n')
        f.write(r'sed -i "s/^\( *\)irun=${irun_n}/\1irun=${irun_m}/" gtc.in'+\
                '\n')
        # Automatically rename the gtc.out file by adding the irun number
        f.write('mv gtc.out gtc.out${irun_n}\n')

def test_job_finish(i, args):
    '''test if the number i queued job is completed

    :param int i: chained job number
    :param int waittime: persistent checking time in minutes
    '''
    gtcoutfile = './gtc.out{0}'.format(i)
    wait=0
    while wait < args.wait_time:
        if os.path.isfile(gtcoutfile):
            return True
        time.sleep(60)
        wait += 1
    print('Job number {0} exceeds '.format(i)+\
          'waiting time limit {0}min.'.format(args.wait_time)+\
          'Please check its status manually.')
    sys.exit(3)

def submit_job(i, args):
    '''submit the written job-script '''
    if args.machine in ['cori', 'edison']:
        submit_cmd = 'sbatch'
    elif args.machine in ['titan']:
        submit_cmd = 'qsub'
    else:
        raise ValueError('Machine [{0}] Not Supported.'.format(args.machine)+\
        ' Write your own job-script writer.'+\
        ' If an existing machine job-script is supported,'+\
        ' add current machine to the available list in submit-GTC.py.'+\
        ' Available machines are {0}'.format(available_machine_list))
    subp.check_call([submit_cmd, args.job_filename])
    print('Job {0} submitted. {1}'.format(i,datetime.now()))

def main():
    ''' create and submit GTC job-scripts corresponding to running environment
'''
    parser = argparse.ArgumentParser(description='submit GTC jobs')

    parser.add_argument('-e', dest='existing_jobfile', action='store_true',
                        help='If specified, will use the existing job script '+\
                             'file. Default: %(default)s')
    parser.add_argument('-m', '--machine', default='cori',
                        choices=available_machine_list,
                        help='Machine to be used. Default: %(default)s')
    parser.add_argument('-T', '--time', default='00:30:00',
                        help='requested wall time. Default: %(default)s')
    parser.add_argument('-p', '--partition', default='debug',
                        help='requested queue. Default: %(default)s')
    parser.add_argument('-N', '--nodes', default=1,type=int,
                        help='requested total node number. Default: %(default)s')
    parser.add_argument('-n', '--nmpi', default=None,type=int,
                        help='requested total MPI processes. '+\
                             'Default: total CPU devided by OpenMP threads.')
    parser.add_argument('-c', '--chain', default=1,type=int,
                        help='total job number to be chained. Default: %(default)s')
    parser.add_argument('--chain-start',dest='chain_start', default=0,type=int,
                        help='starting job/irun number of the chain. Default: %(default)s')
    parser.add_argument('-t','--threads', default=2,type=int,
                        help='OpenMP threads number. Default: %(default)s')
    parser.add_argument('--account', default='m808',
                        help='Charging account code. Default: %(default)s')
    parser.add_argument('--email', default=None,
                        help='your email for receiving notifications. Default: %(default)s')
    parser.add_argument('--job-filename', dest='job_filename',
                        default='job.batch',
                        help='job batch script file name. Default: %(default)s')
    parser.add_argument('--wait-time', dest='wait_time', default=300, type=int,
                        help='Waiting time between chained jobs, in minutes. Default: %(default)s')
    parser.add_argument('--name', default='GTC',
                        help='Waiting time between chained jobs, in minutes. Default: %(default)s')
    parser.add_argument('--workdir', default='./',
                        help='Path to the gtc working directory. Default: %(default)s')
    parser.add_argument('--dry-run', dest='dry_run', action='store_true', 
                        help='Generate the job script file without actually '+\
                             'submit it. Default: %(default)s')
    args = parser.parse_args()

    # test if an existing job file is to be used
    if args.existing_jobfile:
        print('Using existing job file: {0}'.format(args.job_filename))
    else:
        #  if an existing job file is detected, ask the user whether overwrite it
        if os.path.isfile(args.job_filename):
            move = raw_input('Existing job script file {0} detected.\n'.format(args.job_filename) +\
                             'Add -e flag to use existing job file by default.\n'+\
                             'How to proceed? ([Q]uit; [O]verwrite; [U]se existing file)\n')
            if move in ['o','O']:
                print('Existing job file overwritten. New file is used.')
                # write the job script
                write_jobscript(args)
            elif move in ['u', 'U']:
                print('Existing job file used.')
            else:
                print('Stop. No job submitted.')
                sys.exit(1)
        else:
            print('GTC submission parameters:')
            for key,value in vars(args).items():
                print('{0} : {1}'.format(key, value))

            # Double check the parameters before move on
            question = raw_input('Hit Enter to continue(q to quit):')
            if question == 'q':
                print('User Quit.')
                sys.exit(1)

            write_jobscript(args)

    # submit the jobs
    if not args.dry_run:
        for i in range(args.chain_start, args.chain_start + args.chain):
            submit_job(i, args)
            # keep testing if the job is finished until wait_time is reached
            test_job_finish(i, args)


if __name__ == "__main__":
    main()
